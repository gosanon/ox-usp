function spawnLogo() {
  createElement('div') // TODO bg image via "data:"
    .setStyle({
      zIndex: 69,
      position: 'fixed',
      top: '-10px',
      left: '22px',
      width: '260px',
      height: '100px',
      cursor: 'pointer',
      backgroundSize: 'cover'
    })
    .appendTo(document.body)
    .setProperties({
      className: 'urfu-onyx-svg',
      onclick: () => window.open('https://vk.com/gosanon.onyx', '_blank').focus(),
      alt: 'vk.com/gosanon.onyx',
      title: 'vk.com/gosanon.onyx'
    });
}

async function spawnBg() {
  createElement('img') // TODO bg image  via "data:"
    .setProperties({
      src: await chrome.runtime.getURL('./img/bgpattern1.svg')
    })
    .setStyle({
      zIndex: -1,
      pointerEvents: 'none',
      position: 'fixed',
      top: '0px',
      left: '0px',
      height: '100vh'
    })
    .appendTo(document.body);
}

async function changeFavicon() {
  const link = document.querySelector("link[rel~='icon']");
  if (!link) {
    link = document.createElement('link');
    link.rel = 'icon';
    document.head.appendChild(link);
  }
  link.href = await chrome.runtime.getURL('img/favicon.ico');
}

function changeTitle() {
  document.title = 'ЛК УрФУ';
}

function prepareSnackbar() {
  return {
    snackbar: createElement('div')
      .setProperties({ id: 'snackbar' })
      .appendTo(document.body) }
}

function prepareProfileMenu(snackbar) {
  const exitButton = document.querySelector('.user > a.exit');
  const submenuContainer = document.querySelector('.main-data');
  const nameElement = document.querySelector('.user > .stdname');
  const [stNo, grNo] = submenuContainer.querySelectorAll('p');

  stNo.onclick = () => {
    fallbackCopyTextToClipboard(stNo.childNodes[2].textContent);
    snackbar.textContent = 'Номер студенческого скопирован';
    snackbar.className = "show";
    setTimeout(() => snackbar.className = '', 2100);

    // close menu
    submenuContainer.setAttribute('style', 'display: none !important;');
    nameElement.addEventListener('mouseover', openProfileMenu);
    submenuContainer.removeEventListener('mouseleave', handleSubmenuMouseOut);
    nameElement.classList.remove('menuactive')
  }

  grNo.onclick = () => {
    fallbackCopyTextToClipboard(grNo.childNodes[2].textContent);
    snackbar.textContent = 'Номер группы скопирован';
    snackbar.className = "show";
    setTimeout(() => snackbar.className = '', 2100);

    // close menu
    submenuContainer.setAttribute('style', 'display: none !important;');
    nameElement.addEventListener('mouseover', openProfileMenu);
    submenuContainer.removeEventListener('mouseleave', handleSubmenuMouseOut);
    nameElement.classList.remove('menuactive')
  }

  /* For screenshots */
  window.s = () => {
    nameElement.childNodes[2].textContent = 'Жмышенко Валерий Альбертович'
    stNo.childNodes[2].textContent = '94837261'
    grNo.childNodes[2].textContent = 'ЧЕЛ-337228'
  }

  //s()

  exitButton.remove();
  document.querySelector('.student-data-info').appendChild(exitButton);
  exitButton.setAttribute('style', 'display:flex!important')

  submenuContainer.remove();
  nameElement.parentElement.appendChild(submenuContainer);

  function openProfileMenu() {
    submenuContainer.setAttribute('style', 'display: flex !important;');
    submenuContainer.addEventListener('mouseleave', handleSubmenuMouseOut);
    nameElement.removeEventListener('mouseover', openProfileMenu);
    nameElement.addEventListener('mouseleave', handleNameElementMouseOut);
    nameElement.classList.add('menuactive')
  }

  function handleSubmenuMouseOut(e) {
    if (document.elementFromPoint(e.clientX, e.clientY) === nameElement) {
      nameElement.addEventListener('mouseleave', handleNameElementMouseOut);

    } else {
      submenuContainer.setAttribute('style', 'display: none !important;');
      nameElement.addEventListener('mouseover', openProfileMenu);
      submenuContainer.removeEventListener('mouseleave', handleSubmenuMouseOut);
      nameElement.classList.remove('menuactive')
    }
  }

  function handleNameElementMouseOut(e) {
    const elUnderMouse = document.elementFromPoint(e.clientX, e.clientY);
    if (elUnderMouse === submenuContainer
        || elUnderMouse?.parentElement === submenuContainer
        || elUnderMouse?.parentElement?.parentElement === submenuContainer) {
      nameElement.removeEventListener('mouseleave', handleNameElementMouseOut);
    } else {
      submenuContainer.setAttribute('style', 'display: none !important;');
      nameElement.addEventListener('mouseover', openProfileMenu);
      submenuContainer.removeEventListener('mouseleave', handleSubmenuMouseOut);
      nameElement.classList.remove('menuactive')
    }
  }
  
  nameElement.addEventListener('mouseover', openProfileMenu);
}

async function preparePrimarySections() {
  const mainWrapper = (await HTMLFileToDiv('panel.html'))
    .setProperties({ className: 'ox-ui-main-wrapper' })
    .appendTo(document.querySelector('.cont'));

  const scheduleBox = mainWrapper.querySelector('.ox-ui-schedule');
  const prsBox = mainWrapper.querySelector('.ox-ui-prs');

  const scheduleLoader = (await HTMLFileToDiv('img/liquid-loader.html'))
    .appendTo(scheduleBox);
  
  const prsLoader = (await HTMLFileToDiv('img/liquid-loader.html'))
    .appendTo(prsBox);

  return { scheduleBox, prsBox, scheduleLoader, prsLoader };
}

// export
async function doPrestyling() {
  changeTitle();
  spawnLogo();

  const snackbar = prepareSnackbar();
  prepareProfileMenu(snackbar);

  await Promise.all([
    spawnBg(), // todo make sync
    changeFavicon(), // todo make sync
  ]);

  return await preparePrimarySections();
}