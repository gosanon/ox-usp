import AuthInput from "../components/auth-input";
import CustomElementManager from "../CustomElementsManager";
import Route from "../Route";

function extractLoginFields() {
    const [ loginInput, passwordInput ] = Array.from(document.querySelectorAll('input.text'));
    
    loginInput.remove();
    passwordInput.remove();

    return [ loginInput, passwordInput ];
}

function extractAdditionalActions() {
    const actions = Array.from(document.querySelectorAll('.pageLink'));
    actions.forEach(el => el.remove());

    return actions;
}

export default class AuthRoute extends Route {
    getRegex(): RegExp {
        return /https:\/\/sso.urfu.ru\/adfs\/OAuth2\/.*/;
    }

    onRoute(): void {
        const customElementManager = new CustomElementManager(customElements);

        customElementManager.registerComponents(AuthInput);

        const loginFields = extractLoginFields();
        const additionalActions = extractAdditionalActions();

        document.body.innerHTML = '';

        const el = new AuthInput();
        document.body.appendChild(el);

        console.log(el);
    }
}