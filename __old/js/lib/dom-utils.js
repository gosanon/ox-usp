function setProperties (element, props) {
  Object.keys(props).map(prop => {
    element[prop] = props[prop];
  });

  return element;
}

function setStyle (element, style) {
  Object.keys(style).map(name => {
    element.style[name] = style[name];
  });

  return element;
}

function createElement (tag, children = []) {
  let el = document.createElement(tag);

  if (children) {
    children.forEach(item => {
      el.appendChild(item);
      console.log(el.children);
    });
  }

  el.prependTo = parent => { parent.prepend(el); return el; };
  el.appendTo = parent => { parent.appendChild(el); return el; };
	el.setProperties = function (arg) { return setProperties(this, arg); };
	el.setStyle = function (arg) { return setStyle(this, arg); };
  el.displayFlexContentCentered = () => el.setStyle({
    display: 'flex', justifyContent: 'center', alignItems: 'center'
  });
  el.displayFlexContentCenteredHorizontally = () => el.setStyle({
    display: 'flex', justifyContent: 'center'
  });
  el.fillParent = () => el.setStyle({ width: '100%', height: '100%', top: 0, left: 0 });

  return el;
}

function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.style.opacity = 0;
  textArea.value = text;
  
  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}