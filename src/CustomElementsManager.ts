import Component from "./Component";

export default class CustomElementManager {
    private readonly customElementRegistry: CustomElementRegistry;

    constructor(customElementRegistry: CustomElementRegistry) {
        this.customElementRegistry = customElementRegistry;
    }

    registerComponents(...componentsConstructors: typeof Component[]) {
        componentsConstructors.forEach(c => c.register(this.customElementRegistry));
    }
}