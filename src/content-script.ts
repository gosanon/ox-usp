import AuthRoute from "./routes/AuthRoute";
import RootRoute from "./routes/RootRoute";
import RouteManager from "./RouteManager";

const rootManager = new RouteManager();

rootManager.registerRoute(new RootRoute())
rootManager.registerRoute(new AuthRoute())

rootManager.handleUrl(window.location.href);

console.log(window.location.href);
console.log('end');


