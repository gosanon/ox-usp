import Route from "./Route";

export default class RouteManager {
    private routes: Map<RegExp, Route> = new Map();
    
    registerRoute(route: Route) {
        this.routes.set(route.getRegex(), route);
    }

    handleUrl(url: string) {
        for (const routeRegEx of this.routes.keys()) {
            if (routeRegEx.test(url))
                return this.routes.get(routeRegEx)?.onRoute();            
        }
    }
}