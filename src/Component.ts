export default class Component extends HTMLElement {
    protected static customTagName: string;
    protected static customElementConstructor: CustomElementConstructor;

    constructor(tagName: string, customElementConstructor: CustomElementConstructor) {
        super();
        // this.customTagName = tagName;
        // this.customElementConstructor = customElementConstructor;
    }

    static register(customElementsInstance: CustomElementRegistry): void {
        console.log({ customElementsInstance });
        
        customElementsInstance.define(this.customTagName, this.customElementConstructor)
    }
}