import Component from "../Component"

export default class AuthInput extends Component {
    protected static customTagName = 'auth-input'
    protected static customElementConstructor = AuthInput
    
    constructor() {
        super('auth-input', AuthInput)
    }

    connectedCallback() {
        this.innerHTML = `<div>Hello world!</div>`
        alert('here I AM!!!')
    }
}