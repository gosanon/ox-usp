(async () => {
  createElement('div')
    .setProperties({
      className: 'urfu-onyx-svg',
      onclick: () => window.open('https://vk.com/gosanon.onyx', '_blank').focus(),
      alt: 'vk.com/gosanon.onyx',
      title: 'vk.com/gosanon.onyx'
    })
    .setStyle({
      zIndex: 69,
      position: 'fixed',
      top: '-10px',
      left: '22px',
      width: '160px',
      height: '100px',
      backgroundSize: 'cover'
    })
    .appendTo(document.body);

  createElement('img') //TODO bg image  via "data:"
    .setProperties({
      src: await chrome.runtime.getURL('./img/bgpattern1.svg')
    })
    .setStyle({
      zIndex: 0,
      pointerEvents: 'none',
      position: 'fixed',
      top: '0px',
      left: '0px',
      height: '100vh'
    })
    .appendTo(document.body);

  const link = document.querySelector("link[rel~='icon']");
  if (!link) {
    link = document.createElement('link');
    link.rel = 'icon';
    document.head.appendChild(link);
  }
  link.href = await chrome.runtime.getURL('img/favicon.ico');

  document.title = 'ЛК УрФУ (вход)';
})();