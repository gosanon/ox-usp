export default abstract class Route {
    abstract getRegex(): RegExp
    abstract onRoute(): void
}