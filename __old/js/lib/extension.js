// export
async function HTMLFileToDiv(path) {
  /* Must be web-accessible! */
  return createElement('div')
    .setProperties({
      innerHTML: await (await fetch(await chrome.runtime.getURL(path))).text() });
}