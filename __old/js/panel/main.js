(async function main() {
  const { scheduleBox, prsBox, scheduleLoader, prsLoader } = await doPrestyling();
  
  await Promise.all([
    spawnSchedule(scheduleBox, scheduleLoader),
    spawnPrs(prsBox, prsLoader)
  ]);
})();