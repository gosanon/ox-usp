import Route from "../Route";

function signInButtonExists() {
    const signInButton = document.querySelector('.auth');
    return !!signInButton;
}

function redirectToAuth() {
    console.log('get outta here');
    
    document.location.href = "https://sso.urfu.ru/adfs/OAuth2/authorize?resource=https%3A%2F%2Fistudent.urfu.ru&type=web_server&client_id=https%3A%2F%2Fistudent.urfu.ru&redirect_uri=https%3A%2F%2Fistudent.urfu.ru%3Fauth&response_type=code&scope="
}

export default class RootRoute extends Route {
    getRegex(): RegExp {
        return /https:\/\/istudent\.urfu\.ru\/.*/;
    }

    onRoute(): void {
        if (signInButtonExists())
            return redirectToAuth();

        console.log(signInButtonExists());
        
        document.body.style.display = 'block';
    }
}