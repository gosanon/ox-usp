import copy from 'bun-copy-plugin';

await Bun.build({
    entrypoints: [ 'src/content-script.ts' ],
    outdir: 'dist',
    minify: true,
    plugins: [
        copy('src/static/', 'dist/static/'),
        copy('src/manifest.json', 'dist/manifest.json'),
        copy('src/ce.polyfill.js', 'dist/ce.polyfill.js')
    ]
});