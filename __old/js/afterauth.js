/* Styling */
(async () => {
  createElement('div') // TODO bg image via "data:"
    .setStyle({
      zIndex: 69,
      position: 'fixed',
      top: '-10px',
      left: '22px',
      width: '260px',
      height: '100px',
      cursor: 'pointer',
      backgroundSize: 'cover'
    })
    .appendTo(document.body)
    .setProperties({
      className: 'urfu-onyx-svg',
      onclick: () => window.open('https://vk.com/gosanon.onyx', '_blank').focus(),
      alt: 'vk.com/gosanon.onyx',
      title: 'vk.com/gosanon.onyx'
    });

  createElement('img') // TODO bg image  via "data:"
    .setProperties({
      src: await chrome.runtime.getURL('./img/bgpattern1.svg')
    })
    .setStyle({
      zIndex: -1,
      pointerEvents: 'none',
      position: 'fixed',
      top: '0px',
      left: '0px',
      height: '100vh'
    })
    .appendTo(document.body);

  const link = document.querySelector("link[rel~='icon']");
  if (!link) {
    link = document.createElement('link');
    link.rel = 'icon';
    document.head.appendChild(link);
  }
  link.href = await chrome.runtime.getURL('img/favicon.ico');

  document.title = 'ЛК УрФУ';
})();

/* SnackBar */
let snackbar = createElement('div')
  .setProperties({ id: 'snackbar' })
  .appendTo(document.body);
(async () => {
  
})();

/* Profile menu */ //TODO: MAKE ROUND MENU INSTEAD
(async () => {
  const exitButton = document.querySelector('.user > a.exit');
  const submenuContainer = document.querySelector('.main-data');
  const nameElement = document.querySelector('.user > .stdname');
  const [stNo, grNo] = submenuContainer.querySelectorAll('p');

  stNo.onclick = () => {
    fallbackCopyTextToClipboard(stNo.childNodes[2].textContent);
    snackbar.textContent = 'Номер студенческого скопирован';
    snackbar.className = "show";
    setTimeout(() => snackbar.className = '', 2100);

    // close menu
    submenuContainer.setAttribute('style', 'display: none !important;');
    nameElement.addEventListener('mouseover', openProfileMenu);
    submenuContainer.removeEventListener('mouseleave', handleSubmenuMouseOut);
    nameElement.classList.remove('menuactive')
  }

  grNo.onclick = () => {
    fallbackCopyTextToClipboard(grNo.childNodes[2].textContent);
    snackbar.textContent = 'Номер группы скопирован';
    snackbar.className = "show";
    setTimeout(() => snackbar.className = '', 2100);

    // close menu
    submenuContainer.setAttribute('style', 'display: none !important;');
    nameElement.addEventListener('mouseover', openProfileMenu);
    submenuContainer.removeEventListener('mouseleave', handleSubmenuMouseOut);
    nameElement.classList.remove('menuactive')
  }

  /* For screenshots */
  window.s = () => {
    nameElement.childNodes[2].textContent = 'Жмышенко Валерий Альбертович'
    stNo.childNodes[2].textContent = '94837261'
    grNo.childNodes[2].textContent = 'ЧЕЛ-337228'
  }

  //s()

  exitButton.remove();
  document.querySelector('.student-data-info').appendChild(exitButton);
  exitButton.setAttribute('style', 'display:flex!important')

  submenuContainer.remove();
  nameElement.parentElement.appendChild(submenuContainer);

  function openProfileMenu() {
    submenuContainer.setAttribute('style', 'display: flex !important;');
    submenuContainer.addEventListener('mouseleave', handleSubmenuMouseOut);
    nameElement.removeEventListener('mouseover', openProfileMenu);
    nameElement.addEventListener('mouseleave', handleNameElementMouseOut);
    nameElement.classList.add('menuactive')
  }

  function handleSubmenuMouseOut(e) {
    if (document.elementFromPoint(e.clientX, e.clientY) === nameElement) {
      nameElement.addEventListener('mouseleave', handleNameElementMouseOut);

    } else {
      submenuContainer.setAttribute('style', 'display: none !important;');
      nameElement.addEventListener('mouseover', openProfileMenu);
      submenuContainer.removeEventListener('mouseleave', handleSubmenuMouseOut);
      nameElement.classList.remove('menuactive')
    }
  }

  function handleNameElementMouseOut(e) {
    const elUnderMouse = document.elementFromPoint(e.clientX, e.clientY);
    if (elUnderMouse === submenuContainer
        || elUnderMouse?.parentElement === submenuContainer
        || elUnderMouse?.parentElement?.parentElement === submenuContainer) {
      nameElement.removeEventListener('mouseleave', handleNameElementMouseOut);
    } else {
      submenuContainer.setAttribute('style', 'display: none !important;');
      nameElement.addEventListener('mouseover', openProfileMenu);
      submenuContainer.removeEventListener('mouseleave', handleSubmenuMouseOut);
      nameElement.classList.remove('menuactive')
    }
  }
  
  nameElement.addEventListener('mouseover', openProfileMenu);
})();

/* Loaders */
(async () => {
  
});

/* Study schedule */
let scheduleBox, prsBox;

(async () => {
  createElement('div')
    .setProperties({
      className: 'ox-ui-main-wrapper',
      innerHTML: await (await fetch(await chrome.runtime.getURL('panel.html'))).text() })
    .appendTo(document.querySelector('.cont'));

  const loaderUrl = await chrome.runtime.getURL('img/liquid-loader.html');
  const getLoader = async () => createElement('div').setProperties({
    innerHTML: await (await fetch(loaderUrl)).text() });
    
  scheduleBox = document.querySelector('.ox-ui-schedule');
  prsBox = document.querySelector('.ox-ui-prs');
  
  scheduleBox.appendChild(await getLoader());
  prsBox.appendChild(await getLoader());

  // Now, load

  const generateAccessToken = function () {
    let t = '', p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 16; i++) { t += p.charAt(Math.floor(Math.random() * p.length)) }
    return t;
  }
  
  const generateCallbackString = function () {
    return 'jQuery'
      + (/* jquery version */ '2.1.4' + Math.random()).replace(/\D/g, '')
      + '_'
      + +new Date();
  }
  
  const scheduleAccessToken = generateAccessToken();
  const callbackString = generateCallbackString();

  const scheduleRes = await fetch(`https://istudent.urfu.ru/itribe/schedule_its/?access-token=${scheduleAccessToken}&callback=${callbackString}`);
  const schedule = await scheduleRes.json();
  
  /* First, determine schedule case for short schedule block */

  /*
    Case + what to highlight
    1. Schedule is empty for the default period (1-2 weeks)
      - "No schedule for period" message
    2. There are lessons but not today
      - "Schedule for: <DAY> (<tomorrow/after tomorrow/->)" message
      - Schedule for the given day (default highlighting)
    3. There are lessons today, but last's time is over AND no data for later inside default period
      - "Today's schedule (<DAY>)" message
      - Schedule for today (darker "lessons over" highlighting)
      - "No lessons for default period (after today)" message. ?? OPTION TO LOAD MORE ??
    4. There are lessons today, but last's time is over AND there is data for later inside default period
      - [like (2)] "Schedule for: <DAY> (<tomorrow/after tomorrow/->)" message
      - [like (2)] Schedule for the given day (default highlighting)
    5. There are lessons today and there is at least one not over
      - "Today's schedule (<DAY>)" message
      - Schedule for today (default highlighting (for not started lessons), darker highlighting (for finished lessons))
      - Active lesson (IF there is one) in schedule
  */

  /*
    So, the markup is: MSG + SCH? + MSG?
    Schedule event highlighting: NOT_STARTED / IN_PROGRESS / OVER
  */

  let headerMsg = '';
  let
    shouldDisplaySchedule = false,
    scheduleToDisplay = [];
  let footerMsg = '';

  console.log(schedule);

  if (schedule.empty) {
    /* [1.] */
    const periodLastDay = new Date(schedule.end_date * 1000);

    headerMsg = `Расписание на текущий период (по ${periodLastDay.getDate()} ${months_genitive[periodLastDay.getMonth()]}) пустует...`;
  } else {
    const allEventsData = schedule.schedule;
    const todayTimestamp = new Date(schedule.today).setHours(5,0,0,0);
    const eventsTodayIx = allEventsData.map(({ eventDate }) => eventDate).indexOf(todayTimestamp);
    const possibleEventsToday = allEventsData.filter(({ eventDate }) => eventDate === todayTimestamp);

    if (eventsTodayIx === -1) {
      /* [2.] */

    } else {
      const eventsToday = allEventsData[eventsTodayIx].events;
      const finishedEvents = eventsToday.filter(({ eventDate }) => eventDate <= todayTimestamp);//todo filter
      const unfinishedEvents = eventsToday.filter(({ eventDate }) => eventDate > todayTimestamp);//todo filter

      if (unfinishedEvents.length === 0) {
        if (allEventsData.slice(eventsTodayIx + 1).length == 0) {
          /* [3.] */

        } else {
          /* [4.] */

        }
      } else {
        /* [5.] */

      }
    }
  }

  /* Second, choose header and layout for situation 1-5 and fill the layout */

  scheduleBox.firstElementChild.remove();
  createElement('div').setProperties({ textContent: 'sos' }).appendTo(scheduleBox);



  /* Third, prepare UI for extended data */

  /*
    1) Standard 2-week schedule
    2) Schedule by period
    3) Schedule by teacher's name (by period, default - for today)
    4) Schedule by group
  */
  
  // TODO

/* Point-Rating System data */


const prsRes = await fetch('https://istudent.urfu.ru/s/http-urfu-ru-ru-students-study-brs');
const prsHTML = await prsRes.text();

// TODO ADD LOADING ANIMATION FOR THIS
const subjectsData = prsHTML.slice(
  prsHTML.indexOf('<a class="js-service-rating-link" id="'),
  prsHTML.lastIndexOf('</article>'));

//console.log(subjectsData);

console.log(prsBox.firstElementChild);
prsBox.firstElementChild.remove();
createElement('div').setProperties({ textContent: 'xex' }).appendTo(prsBox);

/*
  Filters:
  1. by semester [default: LAST] (add year hint for each option)
  2. include [default: NO] "unchosen" subjects
*/

/*  */
})();
