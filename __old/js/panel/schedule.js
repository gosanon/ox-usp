function generateAccessToken() {
  let t = '', p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (let i = 0; i < 16; i++) { t += p.charAt(Math.floor(Math.random() * p.length)) }
  return t;
}

function generateCallbackString() {
  return 'jQuery'
    + (/* jquery version */ '2.1.4' + Math.random()).replace(/\D/g, '')
    + '_'
    + +new Date();
}

// export
async function spawnSchedule(scheduleBox, scheduleLoader) {
  const scheduleAccessToken = generateAccessToken();
  const callbackString = generateCallbackString();

  const scheduleRes = await fetch(`https://istudent.urfu.ru/itribe/schedule_its/?access-token=${scheduleAccessToken}&callback=${callbackString}`);
  const schedule = await scheduleRes.json();

  /* First, determine schedule case for short schedule block */

  /*
    Case + what to highlight
    1. Schedule is empty for the default period (1-2 weeks)
      - "No schedule for period" message
    2. There are lessons but not today
      - "Schedule for: <DAY> (<tomorrow/after tomorrow/->)" message
      - Schedule for the given day (default highlighting)
    3. There are lessons today, but last's time is over AND no data for later inside default period
      - "Today's schedule (<DAY>)" message
      - Schedule for today (darker "lessons over" highlighting)
      - "No lessons for default period (after today)" message. ?? OPTION TO LOAD MORE ??
    4. There are lessons today, but last's time is over AND there is data for later inside default period
      - [like (2)] "Schedule for: <DAY> (<tomorrow/after tomorrow/->)" message
      - [like (2)] Schedule for the given day (default highlighting)
    5. There are lessons today and there is at least one not over
      - "Today's schedule (<DAY>)" message
      - Schedule for today (default highlighting (for not started lessons), darker highlighting (for finished lessons))
      - Active lesson (IF there is one) in schedule
  */

  /*
    So, the markup is: MSG + SCH? + MSG?
    Schedule event highlighting: NOT_STARTED / IN_PROGRESS / OVER
  */

  let headerMsg = '';
  let
    shouldDisplaySchedule = false,
    scheduleToDisplay = [];
  let footerMsg = '';
  
  console.log(schedule);
  
  if (schedule.empty) {
    /* [1.] */
    const periodLastDay = new Date(schedule.end_date * 1000);
  
    headerMsg = `Событий до ${periodLastDay.getDate()} ${months_genitive[periodLastDay.getMonth()]} нет. Загрузить ещё?`;
  } else {
    const allEventsData = schedule.schedule;
    const todayTimestamp = new Date(schedule.today).setHours(5,0,0,0);
    const eventsTodayIx = allEventsData.map(({ eventDate }) => eventDate).indexOf(todayTimestamp);
    const possibleEventsToday = allEventsData.filter(({ eventDate }) => eventDate === todayTimestamp);
  
    if (eventsTodayIx === -1) {
      /* [2.] */
  
    } else {
      const eventsToday = allEventsData[eventsTodayIx].events;
      const finishedEvents = eventsToday.filter(({ eventDate }) => eventDate <= todayTimestamp);//todo filter
      const unfinishedEvents = eventsToday.filter(({ eventDate }) => eventDate > todayTimestamp);//todo filter
  
      if (unfinishedEvents.length === 0) {
        if (allEventsData.slice(eventsTodayIx + 1).length == 0) {
          /* [3.] */
  
        } else {
          /* [4.] */
  
        }
      } else {
        /* [5.] */
  
      }
    }
  }
  
  /* Second, choose header and layout for situation 1-5 and fill the layout */
  
  scheduleLoader.remove();
  createElement('div')
    .setProperties({ textContent: headerMsg })
    .appendTo(scheduleBox);
  
  /* Third, prepare UI for extended data */
  
  /*
    1) Standard 2-week schedule
    2) Schedule by period
    3) Schedule by teacher's name (by period, default - for today)
    4) Schedule by group
  */

  // todo
} 





